﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TelefonRehberi.DAL;
using TelefonRehberi.Models;

namespace TelefonRehberi.Controllers
{
    public class KullaniciController : Controller
    {
        private TelefonRehberiDbContext db = new TelefonRehberiDbContext();

        // GET: Kullanici
        public ActionResult Index()
        {
            var kullanicilar = db.Kullanicilar.Include(k => k.Birim).Include(k => k.Unvan);
            return View(kullanicilar.ToList());
        }

        // GET: Kullanici/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kullanici kullanici = db.Kullanicilar.Find(id);
            if (kullanici == null)
            {
                return HttpNotFound();
            }
            return View(kullanici);
        }

        // GET: Kullanici/Create
        public ActionResult Create()
        {
            ViewBag.BirimId = new SelectList(db.Birimler, "BirimId", "BirimAd");
            ViewBag.UnvanId = new SelectList(db.Unvanlar, "UnvanId", "UnvanAd");
            return View();
        }

        // POST: Kullanici/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "KullaniciId,AdSoyad,Telefon,Mail,Faks,Adres,BirimId,UnvanId")] Kullanici kullanici)
        {
            if (ModelState.IsValid)
            {
                db.Kullanicilar.Add(kullanici);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.BirimId = new SelectList(db.Birimler, "BirimId", "BirimAd", kullanici.BirimId);
            ViewBag.UnvanId = new SelectList(db.Unvanlar, "UnvanId", "UnvanAd", kullanici.UnvanId);
            return View(kullanici);
        }

        // GET: Kullanici/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kullanici kullanici = db.Kullanicilar.Find(id);
            if (kullanici == null)
            {
                return HttpNotFound();
            }
            ViewBag.BirimId = new SelectList(db.Birimler, "BirimId", "BirimAd", kullanici.BirimId);
            ViewBag.UnvanId = new SelectList(db.Unvanlar, "UnvanId", "UnvanAd", kullanici.UnvanId);
            return View(kullanici);
        }

        // POST: Kullanici/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "KullaniciId,AdSoyad,Telefon,Mail,Faks,Adres,BirimId,UnvanId")] Kullanici kullanici)
        {
            if (ModelState.IsValid)
            {
                db.Entry(kullanici).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.BirimId = new SelectList(db.Birimler, "BirimId", "BirimAd", kullanici.BirimId);
            ViewBag.UnvanId = new SelectList(db.Unvanlar, "UnvanId", "UnvanAd", kullanici.UnvanId);
            return View(kullanici);
        }

        // GET: Kullanici/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kullanici kullanici = db.Kullanicilar.Find(id);
            if (kullanici == null)
            {
                return HttpNotFound();
            }
            return View(kullanici);
        }

        // POST: Kullanici/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Kullanici kullanici = db.Kullanicilar.Find(id);
            db.Kullanicilar.Remove(kullanici);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
