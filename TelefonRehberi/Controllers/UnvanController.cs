﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TelefonRehberi.DAL;
using TelefonRehberi.Models;

namespace TelefonRehberi.Controllers
{
    public class UnvanController : Controller
    {
        private TelefonRehberiDbContext db = new TelefonRehberiDbContext();

        // GET: Unvan
        public ActionResult Index()
        {
            return View(db.Unvanlar.ToList());
        }

        // GET: Unvan/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Unvan unvan = db.Unvanlar.Find(id);
            if (unvan == null)
            {
                return HttpNotFound();
            }
            return View(unvan);
        }

        // GET: Unvan/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Unvan/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "UnvanId,UnvanAd")] Unvan unvan)
        {
            if (ModelState.IsValid)
            {
                db.Unvanlar.Add(unvan);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(unvan);
        }

        // GET: Unvan/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Unvan unvan = db.Unvanlar.Find(id);
            if (unvan == null)
            {
                return HttpNotFound();
            }
            return View(unvan);
        }

        // POST: Unvan/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "UnvanId,UnvanAd")] Unvan unvan)
        {
            if (ModelState.IsValid)
            {
                db.Entry(unvan).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(unvan);
        }

       

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
