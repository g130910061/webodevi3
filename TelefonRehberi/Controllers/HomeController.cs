﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TelefonRehberi.DAL;
using TelefonRehberi.Models;

namespace TelefonRehberi.Controllers
{
    public class HomeController : Controller
    {
       private TelefonRehberiDbContext db = new TelefonRehberiDbContext();

        public ActionResult Index()
        {

            return View();
        }

        [HttpPost]
        public ActionResult Index(string aranan)
        {   
           var kullanicilar = db.Kullanicilar.Where(u=>u.AdSoyad.ToUpper().Contains(aranan.ToUpper())).ToList();
           return View(kullanicilar);
        }
        public ActionResult UnvanaGore()
        {

            return View();
        }

        [HttpPost]
        public ActionResult UnvanaGore(string aranan)
        {
            var kullanicilar = db.Kullanicilar.Where(u => u.Unvan.UnvanAd.ToUpper().Contains(aranan.ToUpper())).ToList();
            return View(kullanicilar);
        }



    }
}