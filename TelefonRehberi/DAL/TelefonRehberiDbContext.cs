﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using TelefonRehberi.Models;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace TelefonRehberi.DAL
{
    public class TelefonRehberiDbContext : DbContext
    {
        public TelefonRehberiDbContext() : base("TelefonRehberiDbContext")
        {

        }
        public DbSet<Kullanici> Kullanicilar { get; set; }

        public DbSet<Unvan> Unvanlar { get; set; }

        public DbSet<Birim> Birimler { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        }


    }
}