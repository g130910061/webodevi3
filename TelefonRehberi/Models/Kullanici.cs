﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TelefonRehberi.Models
{
    public class Kullanici
    {
        public int KullaniciId { get; set; }
        [Required]
        [Display(Name = "Ad Soyad")]
        public string AdSoyad { get; set; }
        [Required]
        public string Telefon { get; set; }
        [Required]
        [EmailAddress]
        public string Mail { get; set; }
        [Required]
        public string Faks { get; set; }
        [Required]
        public string Adres { get; set; }
        [Required]
        [Display(Name = "Birim")]
        public int BirimId { get; set; }
        [Required]
        [Display(Name = "Unvan")]
        public int UnvanId { get; set; }
        public virtual Unvan Unvan { get; set; }
        public virtual Birim Birim { get; set; }

    }
}