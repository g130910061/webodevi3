﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TelefonRehberi.Models
{
    public class Birim
    {
        public int BirimId { get; set; }
        [Required]
        [Display(Name = "Birim")]
        public String BirimAd { get; set; }
        public virtual ICollection<Kullanici> Kullanicilar { get; set; }

    }
}