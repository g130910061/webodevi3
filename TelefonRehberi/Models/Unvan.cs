﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TelefonRehberi.Models
{
    public class Unvan
    {
        public int UnvanId { get; set; }
        [Required]
        [Display(Name = "Unvan")]
        public string UnvanAd { get; set; }
        public virtual ICollection<Kullanici> Kullanicilar { get; set; }
    }
}